<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <div class="header">
    <c:if test="${ empty loginUser }">		<!-- ログインできているかどうかの確認 -->
        <a href="login">ログイン</a>
        <a href="signup">登録する</a>
    </c:if>
    <c:if test="${ not empty loginUser }">
    <div class="profile">
        <div class="name">ようこそ <c:out value="${loginUser.name}" /> さん
        <a href="./">ホーム</a>
        <a href="Contribution">新規投稿画面</a>
        <a href="settings">ユーザー管理画面</a>
        <a href="logout">ログアウト</a></div>
    </div>
    </c:if>
</div>
    <head>
    	<link href="./css/style.css" rel="stylesheet" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>掲示板ホーム</title>
    </head>
    <body>
        <div class="main-contents">

        <center><h1>掲示板システム</h1></center>

		<!-- ログインできているかどうかの確認 -->
        <c:if test="${ empty loginUser }">
          ログインができておりません。<br>
          右上のメニューよりログインをお願い致します<br><br>
    	</c:if>

<div class="messages">
    <c:forEach items="${messages}" var="message">
            <div class="message">
                <div class="account-name">
                    <span class="login_id"><c:out value="${message.login_id}" /></span>
                    <span class="name"><c:out value="${message.name}" /></span>
                </div>
                <div class="text"><c:out value="${message.messages_text}" /></div>
                <div class="date"><fmt:formatDate value="${message.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
            </div>
    </c:forEach>
</div>
            <div class="form-area">
    <c:if test="${ isShowMessageForm }">
        <form action="newMessage" method="post">
            返信用本文<br />
            <textarea name="message" cols="100" rows="5" class="tweet-box"></textarea>
            <br />
            <input type="submit" value="つぶやく">（140文字まで）
        </form>
    </c:if>
			</div> <!-- form-areaの終了箇所 -->
        </div> <!-- main-contentsの終了箇所 -->

    </body>
</html>
