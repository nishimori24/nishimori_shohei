<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ユーザー新規登録</title>
    </head>
    <body>
        <div class="main-contents">
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
            <form method="post">
                <br />
				・下記の項目を入力して下さい<br><br>
                <label for="login_id">■ログインID</label> <input name="login_id" />
                <br />
                <label for="password">■パスワード</label> <input name="password" type="password" />
                <br />
                <label for="name">■名前</label> <input name="name" />
                <br />
                <label for="branch_number">■支店番号</label> <input name="branch_number" />
                <br />
                <label for="position_number">■部署・役職番号</label> <input name="position_number" />
                <br><br>
                <input type="submit" value="登録" /> <br><br> <a href="./">戻る</a>
            </form>
        </div>
    </body>
</html>