<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規投稿画面</title>
</head>
<body>
新規投稿画面
<br><br>
<div class="main-contents">
            <form action="Contribution" method="post">
                <br>
				・下記の項目を入力して下さい<br><br>
                <label for="post_name">■件名</label> <input name="post_name" />
                <br>
                <label for="post_category">■カテゴリー</label> <input name="post_category" />
                <br>
                <label for="post_text">■本文</label> <input name="post_text" />
                <br><br><br>
                <input type="submit" value="投稿" /> <br><br> <a href="./">戻る</a>
            </form>
        </div>
</body>
</html>