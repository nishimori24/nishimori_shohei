package nishimori_shohei.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nishimori_shohei.beans.User;
import nishimori_shohei.beans.UserContribution;
import nishimori_shohei.service.ContributionService;

@WebServlet(urlPatterns = { "/Contribution" })
public class ContributionServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("Contribution.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();

        if (isValid(request, messages) == true) {

        	UserContribution user = new UserContribution();

        	User userid = (User) session.getAttribute("loginUser");
//        	user.setUser_id(userid.getId());
            user.setPost_name(request.getParameter("post_name"));
            user.setPost_text(request.getParameter("post_text"));
            user.setPost_category(request.getParameter("post_category"));
            new ContributionService().register(user);

            response.sendRedirect("index");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("signup");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}
