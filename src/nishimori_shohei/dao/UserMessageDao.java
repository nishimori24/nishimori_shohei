package nishimori_shohei.dao;

import static nishimori_shohei.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import nishimori_shohei.beans.UserMessage;
import nishimori_shohei.beans.UserPost;
import nishimori_shohei.exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserPost> getUserPost(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("posts.id as id, ");
            sql.append("posts.user_id as user_id, ");
            sql.append("posts.post_name as name, ");
            sql.append("posts.post_category as category, ");
            sql.append("posts.post_text as text, ");
            sql.append("users.name as name, ");
            sql.append("posts.created_date as created_date ");
            sql.append("FROM posts ");
            sql.append("INNER JOIN users ");
            sql.append("ON posts.user_id = users.id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserPost> ret = toUserPostList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserPost> toUserPostList(ResultSet rs)
            throws SQLException {

        List<UserPost> ret = new ArrayList<UserPost>();
        try {
            while (rs.next()) {
                String post_name = rs.getString("post_name");
                String post_category = rs.getString("post_category");
                int id = rs.getInt("id");
                int user_id = rs.getInt("user_id");
                String post_text = rs.getString("post_text");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserPost message = new UserPost();
                message.setPost_name(post_name);
                message.setPost_category(post_category);
                message.setId(id);
                message.setUser_Id(user_id);
                message.setPost_text(post_text);
                message.setCreated_date(createdDate);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }




    private List<UserMessage> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
                String login_id = rs.getString("login_id");
                String name = rs.getString("name");
                int id = rs.getInt("id");
                int userId = rs.getInt("user_id");
                String messages_text = rs.getString("text");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserMessage message = new UserMessage();
                message.setLogin_id(login_id);
                message.setName(name);
                message.setId(id);
                message.setUserId(userId);
                message.setMessages_text(messages_text);
                message.setCreated_date(createdDate);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}