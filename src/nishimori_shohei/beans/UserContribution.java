package nishimori_shohei.beans;

import java.io.Serializable;
import java.util.Date;

public class UserContribution implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private int User_id;
    private String post_name;
    private String post_category;
    private String post_text;
    private Date createdDate;
    private Date updatedDate;

    public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
    public String getPost_name() {
		return post_name;
	}
	public void setPost_name(String post_name) {
		this.post_name = post_name;
	}
	public String getPost_category() {
		return post_category;
	}
	public void setPost_category(String post_category) {
		this.post_category = post_category;
	}
	public String getPost_text() {
		return post_text;
	}
	public void setPost_text(String post_text) {
		this.post_text = post_text;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}
