package nishimori_shohei.service;


import static nishimori_shohei.utils.CloseableUtil.*;
import static nishimori_shohei.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import nishimori_shohei.beans.Message;
import nishimori_shohei.beans.UserPost;
import nishimori_shohei.dao.MessageDao;
import nishimori_shohei.dao.UserMessageDao;

public class MessageService {

    public void register(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();

            MessageDao messageDao = new MessageDao();
            messageDao.insert(connection, message);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    private static final int LIMIT_NUM = 1000;

    public List<UserPost> getPost() {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		UserMessageDao messageDao = new UserMessageDao();
    		List<UserPost> ret = messageDao.getUserPost(connection, LIMIT_NUM);

    		commit(connection);

    		return ret;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }

}