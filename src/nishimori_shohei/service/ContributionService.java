package nishimori_shohei.service;

import static nishimori_shohei.utils.CloseableUtil.*;
import static nishimori_shohei.utils.DBUtil.*;

import java.sql.Connection;

import nishimori_shohei.beans.UserContribution;
import nishimori_shohei.dao.UserDao;

public class ContributionService{

	public void register(UserContribution user) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            userDao.insert(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}

}
