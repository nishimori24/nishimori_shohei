package nishimori_shohei.service;

import static nishimori_shohei.utils.CloseableUtil.*;
import static nishimori_shohei.utils.DBUtil.*;

import java.sql.Connection;

import nishimori_shohei.beans.User;
import nishimori_shohei.dao.UserDao;
import nishimori_shohei.utils.CipherUtil;

public class LoginService {

	public User login(String login_id, String password) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			String encPassword = CipherUtil.encrypt(password);
			User user = userDao.getUser(connection, login_id, encPassword);

			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}